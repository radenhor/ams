
function pausecomp(millis)
{
    var date = new Date();
    var curDate = null;
    do { curDate = new Date(); }
    while(curDate-date < millis);
}

function getAllArticle(p){
    $('tbody').html('<tr><td width="100%" colspan="5"><center><img src="/image/loading.gif" width="80px"></center></td></tr>"');
    $.ajax({
        type: "GET",
        url: "http://api-ams.me/v1/api/articles?page="+p+"&limit=10",
        success: function (response) {
            $('tbody').empty();
            var j = 0;
            var page = $('#page')
            page.empty()
            let data = response.PAGINATION
            let totalPage = data.TOTAL_PAGES
            if(totalPage>1){
                var li = $('<li></li>')
                li.addClass('page-item')
                if(p==1)li.addClass('disabled')
                var a = $('<a></a>').text('Previous')
                a.addClass("page-link")
                a.addClass('page-pre')
                li.append(a)
                page.append(li);
                for(var i=1;i<=totalPage;i++){
                    var li = $('<li></li>')
                    if(i==p)li.addClass('active')
                    var a = $('<a></a>').text(i)
                    a.addClass('page-num')
                    a.addClass("page-link")
                    li.append(a)
                    page.append(li);
                }
                var li = $('<li></li>')
                li.addClass('page-item')
                if(p==totalPage)li.addClass('disabled')
                var a = $('<a></a>').text('Next')
                a.addClass('page-next')
                a.addClass("page-link")
                li.append(a)
                page.append(li);

                $('.page-num').click(function(){
                    $('li').removeClass('active')
                    $(this).parent().addClass('active')
                    var page = $(this).text()
                    console.log("PAGE : " + page)
                    getAllArticle(page)
                })

                $('.page-pre').click(()=>{
                    p = p - 1
                    if(p>=1){
                        getAllArticle(p)
                    }else{
                        p++;
                    }
                })
                $('.page-next').click(()=>{
                    p = p + 1
                    if(p<=totalPage){
                        getAllArticle(p)
                    }else{
                        p--;
                    }
                    
                })
            }
            for(data of response.DATA){
                j++;
                if(data.IMAGE==null){
                    data.IMAGE = 'https://grnpb.com/data/profile/cowo.png'}
                renderTable(j,data.ID,data.TITLE,data.DESCRIPTION,data.IMAGE);
            }
            // $('#dataTable').DataTable();
        },
        error : function(response){
            console.log(response);
        }
    });
}
getAllArticle(1,1)

function renderTable(no,id,title,desc,url){
    var row = $('<tr></tr>')
    var rowNo = $('<td></td>').text(id)
    var rowTitle = $('<td></td>').text(title)
    var rowDesc = $('<td></td>').text(desc)
    var rowAction = $('<td></td>')
    var rowImg = $('<td></td>')
    var btnDelete = $('<button></button>').text("Delete")
    var btnUpdate = $('<button></button>').text("Update")
    var img = $('<img></img>')
    img.attr('src',url)
    img.attr('width','30px')
    rowImg.append(img)
    btnDelete.addClass('btn btn-danger')
    btnDelete.css('margin-right','10px')
    btnDelete.attr('id',id)
    btnDelete.attr('value',id)
    btnDelete.attr('data-toggle',"modal")
    btnDelete.attr('data-target','#deleteArticle')
    btnUpdate.addClass('btn btn-primary')
    btnUpdate.attr('id',id)
    btnUpdate.attr('value',id)
    btnUpdate.attr('data-target','#updateArticle')
    btnUpdate.attr('data-toggle',"modal")
    btnUpdate.click(()=>{
        var uid = btnUpdate.val()
        $('#articleID').attr('value',uid)
        $('#errDesc').text('')
        $('#errTitle').text('')
        $('#title').val(title)
        $('#desc').val(desc)
        // getArticleById(id)
        $('#update').text('Update')
        $('#update').click(()=>{
            if($('#update').text()=='Update'){
                let title = $('#title').val()
                let desc = $('#desc').val()
                if(title==""){
                    $('#errTitle').text('* Invalid Input')
                }else{
                    $('#errTitle').text('')
                }
                if(desc==""){
                    $('#errDesc').text('* Invalid Input')
                }else{
                    $('#errDesc').text('')
                }
                if(title!=""&&desc!=""){
                    updateArticle($('#articleID').val(),$('#title').val(),$('#desc').val())
                    $('#updateArticle').modal('hide')
                    $('#title').val('')
                    $('#desc').val('')
                }
            }
            
        })
        $('#cancel').click(()=>{
            $('#title').val('')
            $('#desc').val('')
            $('#errDesc').text('')
            $('#errTitle').text('')
        })
    })
    rowAction.append(btnDelete,btnUpdate);
    row.append(rowNo,rowTitle,rowImg,rowDesc,rowAction)
    $('tbody').append(row)
    btnDelete.click(()=>{
        var uid = btnUpdate.val()
        $('#delete').click(()=>{
            deleteArticle(uid)
        })
    })
}

function deleteArticle(id){
    $.ajax({
        type: "DELETE",
        url: "http://api-ams.me/v1/api/articles/"+id,
        success: function (response) {
            console.log(response)
            getAllArticle(1)
        },
        error: function(response){
            console.log(response)
        }
    });
}

function updateArticle(id,title,desc){
    let data = {
        "TITLE": title,
        "DESCRIPTION": desc,
      }
    $.ajax({
        type: "PUT",
        url: "http://api-ams.me/v1/api/articles/"+id,
        data: JSON.stringify(data),
        headers : {
            'Content-Type' : 'application/json'
        },
        success: function (response) {
            console.log(response)
            getAllArticle(1)
           
        },
        error: function(response){
            console.log(response)
        }
    });
}

function getArticleById(id){
    $.ajax({
        type: "GET",
        url: "http://api-ams.me/v1/api/articles/"+id,
        success: function (response) {
            $('#title').val(response.DATA.TITLE)
            $('#desc').val(response.DATA.DESCRIPTION)
        },
        error: function(response){
            console.log(response)
        }
    });
}

function addArticle(title,desc){
    let data = {
        "TITLE": title,
        "DESCRIPTION": desc,
    }
    $.ajax({
        type: "POST",
        url: "http://api-ams.me/v1/api/articles",
        data: JSON.stringify(data),
        success: function (response) {
            console.log(response)
            getAllArticle(1)
        },
        headers : {
            'Content-Type' : 'application/json'
        },
        error: function(response){
            console.log(response)
        }
    });
}
$('#insert').attr('data-target','#updateArticle')
$('#insert').attr('data-toggle',"modal")
$('#insert').click(()=>{
    $('#errTitle').text('')
    $('#errDesc').text('')
    $('#update').text('Insert')
    $('#update').click(()=>{
        if($('#update').text()=='Insert'){
            let title = $('#title').val()
            let desc = $('#desc').val()
            if(title==""){
                $('#errTitle').text('* Invalid Input')
            }else{
                $('#errTitle').text('')
            }
            if(desc==""){
                $('#errDesc').text('* Invalid Input')
            }else{
                $('#errDesc').text('')
            }
            if(title!=""&&desc!=""){
                addArticle(title,desc)
                $('#updateArticle').modal('hide')
                $('#title').val('')
                $('#desc').val('')
            }
        }
        
    })
    $('#cancel').click(()=>{
        $('#title').val('')
        $('#desc').val('')
        $('#errDesc').text('')
        $('#errTitle').text('')
    })
})


function deleteAll(){
    $.ajax({
        type: "GET",
        url: "http://api-ams.me/v1/api/articles?page=1&limit=15",
        success: function (response) {
            for(data of response.DATA){
                $.ajax({
                    type: "DELETE",
                    url: "http://api-ams.me/v1/api/articles/"+data.ID,
                    success: function (response) {
                        console.log(response)
                    },
                    error: function(response){
                        console.log(response)
                    }
                });
            }
            getAllArticle(1)
            // $('#dataTable').DataTable();
        },
        error : function(response){
            console.log(response);
        }
    });
}

// setInterval(function(){
//     getAllArticle()
//     console.log("HEllo");
// },1000)


                // var li = $('<li></li>')
                // li.addClass('page-item disabled')
                // var a = $('<a></a>').text('Previous')
                // a.addClass("page-link")
                // li.append(a)
                // page.append(li);
                
                // var li = $('<li></li>')
                // li.addClass('page-item')
                // var a = $('<a></a>').text('Next')
                // a.addClass("page-link")
                // li.append(a)
                // page.append(li);
            
     

// $("#page").on("click", "li", (function() {
//     var page = $(this).index()+1;
//     // $('li').removeClass('active')
//     $(this).addClass("page-link active")
//     console.log($(this));
//     getAllArticle(page)
// })
// );



function addMulti(){
    for(var i=0;i<50;i++){
        addArticle("Test "+(i+1),"Test "+(i+1))
    }
}

// $(document).ready(function(){
//     $('.page-num').click(function(){
//         alert('ll')
//         $('li').removeClass('active')
//         $(this).parent().addClass('active')
//         var page = $(this).text()
//         console.log("PAGE : " + page)
//         getAllArticle(page)
//     })
// })


